from AtualizarBancoDeDados import *
import json
import uuid
from flask import request

class Usuario:
    def __init__(self, user_id):
        self.user_id = user_id
        self.contexto = []
        self.itensEscolhidos = {}
        self.filaMensagens = []
       

    def enviarMensagemFB(self, payload):
        self.filaMensagens.append(payload)

class PostBack():
    def __init__(self, dados, funcao):
        self.id = str(uuid.uuid4())
        self.dados = dados
        self.callback = funcao
    

class ProcessadorMensagens:
    def __init__(self):
        self.ConexaoBanco = psycopg2.connect(DATABASE_URL)
        self.postbackValidos = {}
        self.postbacks = {}
        self.resumosFinalizados = {}

    def addPostback(self, postback):
        self.postbacks[postback.id] = postback

    def consultaItensFiltrandoPorTag(self, tags, tipoitem):
 
        consultaTags   = self.ConexaoBanco.cursor()

        traducao_tipoid = {
             'Memória':1,
             'Processador':2,
             'Placa mãe':3,
             'Placa de vídeo':4,
             'HD':5,
             'indefinido': -1
        }

        if tipoitem not in traducao_tipoid:
            return []

        if len(tags) > 0:
            consulta = "SELECT \"ItemTag\".\"id\" FROM \"ItemTag\" INNER JOIN \"CaracteristicaItem\" ON \"CaracteristicaItem\".\"id\" = \"ItemTag\".\"CaracteristicaId\"  WHERE LOWER(\"ItemTag\".\"String\") in (' ' %s)" % (', LOWER(%s)'*len(tags))
            consulta += ' AND "CaracteristicaItem"."Tipo" = '+ str(traducao_tipoid[tipoitem])
            consultaTags.execute(consulta, tags)
        else:
            consulta = "SELECT \"ItemTag\".\"id\" FROM \"ItemTag\" INNER JOIN \"CaracteristicaItem\" ON \"CaracteristicaItem\".\"id\" = \"ItemTag\".\"CaracteristicaId\"  WHERE \"CaracteristicaItem\".\"Tipo\" = %s"
            consultaTags.execute(consulta, [str(traducao_tipoid[tipoitem])])

    
        tagsId = consultaTags.fetchall()
    
        if len(tags) > 0:
            consultaString = 'SELECT "Descricao", "Preco", "Loja", "Tipo", "URL", "id", "ImagemURL" FROM "Item" WHERE 1=1 '
            for tag in tagsId:
                consultaString += 'AND EXISTS(SELECT 1 FROM "RelacionamentoItemTag" WHERE "ItemId" = "id" and "TagId" = ' + str(int(tag[0])) + ')'
    
            consultaString += 'order by "Preco" LIMIT 5'
        else:
            consultaString = 'SELECT "Descricao", "Preco", "Loja", "Tipo", "URL", "id", "ImagemURL" FROM "Item" WHERE 0=1 '
            for tag in tagsId:
                consultaString += 'OR EXISTS(SELECT 1 FROM "RelacionamentoItemTag" WHERE "ItemId" = "id" and "TagId" = ' + str(int(tag[0])) + ')'
    
            consultaString += 'order by "Preco" LIMIT 5'

        consultaItens = self.ConexaoBanco.cursor()
        consultaItens.execute(consultaString)

        return consultaItens.fetchall()
    
    def selecionarItemCompra(self, postback, usuario):
        traducao_tipoid = {
             1: 'Memória RAM',
             2: 'CPU',
             3: 'Placa mãe',
             4: 'Placa de vídeo',
             5: 'HD'
        }
        
        cursorTipoItem = self.ConexaoBanco.cursor()
        cursorTipoItem.execute('SELECT "Item"."Tipo", "Item"."Preco" FROM "Item" WHERE "Item"."id" = %s', [postback.dados['itemId']])
        tipoItem = cursorTipoItem.fetchone()
        
        usuario.itensEscolhidos[tipoItem[0]] = {'id':postback.dados['itemId'], 'preco':  tipoItem[1]}
        usuario.enviarMensagemFB({
                                    "messaging_type": "RESPONSE", 
                                    "recipient": {"id": usuario.user_id}, 
                                    "message":{"text": 'Foi adicionada a ' + traducao_tipoid[tipoItem[0]] + ' na lista de compras.'}
                                })
        usuario.enviarMensagemFB({
                                    "messaging_type": "RESPONSE", 
                                    "recipient": {"id": usuario.user_id}, 
                                    "message":{"text": 'Para ver os itens escolhidos até o momento, digite \"lista parcial\".\nPara finalizar a lista agora, digite \"finalizar lista\"'}
                                })

        soma_dinheiro = 0.0
        for coisa in usuario.itensEscolhidos.keys():
            soma_dinheiro += usuario.itensEscolhidos[coisa]['preco']

        usuario.enviarMensagemFB({
                                    "messaging_type": "RESPONSE", 
                                    "recipient": {"id": usuario.user_id}, 
                                    "message":{"text": 'Subtotal: R$' + str(soma_dinheiro)}
                                })

        print('itens no momento: ' + str( usuario.itensEscolhidos))
        return

    def mostrarGastos(self, parametros, usuario):
        traducao_tipoid = {
             1: 'Memória RAM',
             2: 'CPU',
             3: 'Placa mãe',
             4: 'Placa de vídeo',
             5: 'HD'
        }

        dados_resumidos = []

        soma_dinheiro = 0.0
        for coisa in usuario.itensEscolhidos.keys():
            soma_dinheiro += usuario.itensEscolhidos[coisa]['preco']

        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": usuario.user_id}, "message":{"text": 'Itens escolhidos até o momento:'}})
        for coisa in usuario.itensEscolhidos.keys():
            cursorTipoItem = self.ConexaoBanco.cursor()
            cursorTipoItem.execute('SELECT "Item"."Descricao", "Item"."Preco", "Item"."URL" FROM "Item" WHERE "Item"."id" = %s', [usuario.itensEscolhidos[coisa]['id']])
            dados = cursorTipoItem.fetchone()
            dados_resumidos.append({'Descricao': dados[0], 'Preco': dados[1], 'URL': dados[2] })
            usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": usuario.user_id}, "message":{"text": traducao_tipoid[coisa] + ':' + dados[0] + ' - R$' + str(dados[1])}})
        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": usuario.user_id}, "message":{"text": 'Total: R$' + str(soma_dinheiro) }})
        return dados_resumidos


    def pesquisaPrecoItem(self, parametros, usuario):

        tipoItem = parametros['ComponentComputador'] #obrigatório
        tags     = parametros['CaracteristicaMemoria2'] #TODO: tratar caso que não vem nenhuma tag

        itens = self.consultaItensFiltrandoPorTag(tags, tipoItem)

        if len(itens) == 0:
            usuario.enviarMensagemFB({
                                            "messaging_type": "RESPONSE", 
                                            "recipient": {"id": usuario.user_id}, 
                                            "message":{"text": "Nenhum resultado encontrado. Tente novamente com outros termos de busca."}
                                        })
            return
        
        usuario.enviarMensagemFB({
                                    "messaging_type": "RESPONSE", 
                                    "recipient": {"id": usuario.user_id}, 
                                    "message":{"text": "Exibindo a seguir 5 itens de melhor preço com essas características:"}
                                })

        EstruturaRetorno = {
            "messaging_type": "RESPONSE", 
            "recipient": {"id": usuario.user_id}, 
            "message":{
               "attachment":{
                   "type":"template",
                   "payload": {
                        "template_type":"generic",
                        "elements":[]
                   }
               }
            }
        }

        if "montarumsistema-followup" in usuario.contexto:
            for i, item in enumerate(itens):
                global postbacksRegistrados
                novo_postback = PostBack({"itemId": item[5]}, self.selecionarItemCompra)
                self.postbacks[novo_postback.id] = novo_postback
                EstruturaRetorno['message']['attachment']['payload']['elements'].append({
                                                                                            "title":item[0],
                                                                                            "subtitle": 'R$' + str(item[1]) + ' - ' + item[2],
                                                                                            "image_url": item[6],
                                                                                            "buttons":[{"type": "postback", "title": "Selecionar o item " + str(i+1), "payload": novo_postback.id}]      
                                                                                        })
        else:
            for i, item in enumerate(itens):
                EstruturaRetorno['message']['attachment']['payload']['elements'].append({
                                                                                            "title":item[0],
                                                                                            "subtitle": 'R$' + str(item[1]) + ' - ' + item[2],
                                                                                            "image_url": item[6]
                                                                                        })
        usuario.enviarMensagemFB(EstruturaRetorno)


    def MontarPc(self, parametros, usuario):
        usuario.contexto = ["montarumsistema-followup"]
        usuario.itensEscolhidos = {}
        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Vamos fazer uma lista de componentes."}})
        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Pesquise normalmente por cada item (ram, cpu, memória, hd e fonte) e irá aparecer opções para popular a lista de compras."}})
        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Quando terminar, é só indicar (Ex: \"Terminar lista\")."}})
        return

    def TerminarOrc(self, parametros, usuario):
        chave_nova = str(uuid.uuid4())
        self.resumosFinalizados[chave_nova] = self.mostrarGastos(parametros, usuario)


        usuario.contexto = []
        usuario.itensEscolhidos = {}
        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Configuração terminada."}})
        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Você pode usar o seguinte link para compartilhar:  " + request.base_url + 'resumo/' + chave_nova }})

    def boasVindas(self, parametros, usuario):
        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Bom dia! Com este bot você pode fazer pesquisa de preço de itens de hardware, basta pedir as características relevantes (Ex: \"Preço de uma memória ddr4\""}})
        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Também é possível fazer uma lista selecionando vários itens (ram, cpu, memória, hd e fonte) e gerar um link no final. (Ex: \"gerar lista\")"}})
        return

    def oQueFalta(self, parametros, usuario):
        traducao_tipoid = {
             1: 'Memória RAM',
             2: 'CPU',
             3: 'Placa mãe',
             4: 'Placa de vídeo',
             5: 'HD'
        }

        itens_ja_escolhidos = []
        itens_a_escolher = []
        for chave in traducao_tipoid.keys():
            if chave in usuario.itensEscolhidos:
                itens_ja_escolhidos.append(traducao_tipoid[chave])
            else:
                itens_a_escolher.append(traducao_tipoid[chave])
        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Itens já selecionados: " + ",".join([str(x) for x in itens_ja_escolhidos]) }})
        usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Itens a selecionar: "  + ",".join([str(x) for x in itens_a_escolher])}})
        return
                

    def ProcessaMensagem(self, usuario, EntradaDialogflow):        
        Retorno = None
       
        ProcessadoresIntent = {
            '8a429ad6-0f21-4d66-8ffc-10335a6497fc': self.pesquisaPrecoItem,
            'ab33263c-c312-40ed-bba3-31f1f792d5a8': self.MontarPc,
            '8c61dc0b-6834-4b57-ba9f-1ca0700e4291': self.mostrarGastos,
            '3bcde02d-567b-4bb7-98c9-a58935fbec16': self.TerminarOrc,
            'accf02a8-c9e3-4a18-a3f2-9c894a72fdc6': self.boasVindas,
            'cae9cca5-3981-4401-9776-897bdadea094': self.oQueFalta
        }
    
        print("processando intent")
        if EntradaDialogflow['result']['metadata']['intentId'] in ProcessadoresIntent:
            ProcessadoresIntent[EntradaDialogflow['result']['metadata']['intentId']](EntradaDialogflow['result']['parameters'], usuario)
        else:
            usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Desculpe, mas não entendi."}})
        
        
        return

    def ProcessaPostback(self, usuario, postback_id):
        if postback_id not in self.postbacks:
            usuario.enviarMensagemFB({"messaging_type": "RESPONSE", "recipient": {"id": ''}, "message": {"text": "Este botão já expirou."}})
            return

        self.postbacks[postback_id].callback(self.postbacks[postback_id], usuario)
        self.postbacks = {}
        

