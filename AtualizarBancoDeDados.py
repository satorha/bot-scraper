from scrapers import *
import re
import operator
import os
import psycopg2
DATABASE_URL = os.environ.get('DATABASE_URL')
import unidecode


ConexaoBanco = None


#faz pesquisas usando os scrapers disponíveis e retorna uma lista de objetos.
#cada elemento dessa lista tem o formato: {'Nome': nome, 'Preco': preco, 'URL': url}
def PegarItens():
    ItensEncontrados = []
    stringsBusca = ["placa de video nvidia", "placa de video amd", "processador amd", "processador intel", "ddr3", "ddr4", "memoria 8gb", "memoria 4gb", "hd 500gb", "hd 1tb", "hd 4tb", "placa mae"]

    
    #executa todas as buscas em cada website com scraper disponível
    for loja in scrapersDisponiveis.keys():
        for busca in stringsBusca:
            ItensEncontrados.extend(scrapersDisponiveis[loja](busca))
    
    return ItensEncontrados

def calcularTipoPelaString(item_desc):
    traducao_tipoid = {
             'Memoria':1,
             'Processador':2,
             'Placa-mae':3,
             'Placa de video':4,
             'HD':5
    }

    menor = [-1, 999999999]
    for item in traducao_tipoid:
        atual = unidecode.unidecode(item_desc).lower().find(item.lower())
        if(atual == -1):
            continue
        if atual < menor[1]:
            menor = [traducao_tipoid[item], atual]
    return menor




def CalcularTags(item, listaTags):
    tagsEncontradas = []
    caracteristicasJaUtilizadas = dict()
    contadorTipo = dict()
    for tag in listaTags:
        if item['Nome'].lower().find(tag[1].lower()) != -1:
            if tag[3] in caracteristicasJaUtilizadas:
                continue
            caracteristicasJaUtilizadas[tag[3]] = 1
            tagsEncontradas.append([tag[0], tag[2]])
            contadorTipo[tag[2]] = contadorTipo.setdefault(tag[2], 0) + 1

    if len(tagsEncontradas) == 0:
        return None
    #filtra o objeto para o tipo mais provável
    if len(contadorTipo.items()) == 0:
        return None
    tipo_mais_provavel = calcularTipoPelaString(item['Nome'])

    if tipo_mais_provavel[0] == -1:
        return None

    return [[x[0] for x in tagsEncontradas if x[1] == tipo_mais_provavel[0]], tipo_mais_provavel[0]]



def AtualizarBanco():
    global ConexaoBanco
    ConexaoBanco = psycopg2.connect(DATABASE_URL)


    Itens = PegarItens()

    #classifica os itens lidos
    cursorTags = ConexaoBanco.cursor()
    cursorTags.execute('SELECT "ItemTag"."id", "ItemTag"."String", "CaracteristicaItem"."Tipo", "ItemTag"."CaracteristicaId" FROM "ItemTag" INNER JOIN "CaracteristicaItem" ON "CaracteristicaItem"."id" = "ItemTag"."CaracteristicaId"')
    tags = cursorTags.fetchall()

    cursorSalvarItens = ConexaoBanco.cursor()
    cursorSalvarItens.execute('DELETE FROM "RelacionamentoItemTag"')
    cursorSalvarItens.execute('DELETE FROM "Item"')
    for item in Itens:
        tagsCalculadas = CalcularTags(item, tags)

        if tagsCalculadas is None:
            continue
        
        #grava a informação no banco de dados
        #começa gravando dados principais do item
        cursorSalvarItens.execute('INSERT INTO "Item"("Preco", "URL", "Loja", "Tipo", "Descricao", "ImagemURL") values (%s, %s, %s, %s, %s, %s) RETURNING id', [item['Preco'], item['URL'], item['Loja'], tagsCalculadas[1], item['Nome'], item['Imagem']])
        itemId = cursorSalvarItens.fetchone()[0]
        #grava todas as tags deste item
        for tag in tagsCalculadas[0]:
            cursorSalvarItens.execute('INSERT INTO "RelacionamentoItemTag"("ItemId", "TagId") values (%s, %s)', [itemId, tag])
    ConexaoBanco.commit()
    ConexaoBanco.close()
    print("Fim da atualização dos dados")
