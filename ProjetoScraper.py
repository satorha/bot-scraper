from AtualizarBancoDeDados import *
import os
from flask import Flask, request, render_template
import json
from ProcessadorMensagens import *
import psycopg2
import requests

app = Flask(__name__)

DIALOGFLOW_CONFIG = {'ProjectId': 'newagent-27123', 'URL':'https://api.dialogflow.com/v1/query', 'token': '5e56f3aa50004cbf9fe02244adc2e205'}

FACEBOOK_PAGE_ACCESS_TOKEN = 'EAAee77R4FI0BAM9W3OSYwGfzXKz1ZAbYFdRBSs234YZCg2xqcDalRFev4NNpKJbuD8hMvkJMKNik5MLq4ZAQ3k37YZCPEZBsqnBKYoZAY6RExl1hGSPR5wmPSSRqgAvZAZBZByzlnhsAbVTQFvIZA0AMD2CzmiZCzg96FXIEBIYj9QpZBAZDZD'
FACEBOOK_VERIFY_TOKEN = 'bot_teste_sel'
FB_API_URL = 'https://graph.facebook.com/v2.6/me/messages'


#@app.route('/', methods=['GET', 'POST'])
#def receive_message():
#    if request.method == 'GET':
#       return "bot ok"
#    elif request.method == 'POST':
#        print('dados do request: ' + str(request.data))
#        return ProcessaMensagem(json.loads(request.data))
#    return "Bot funcionando"
Usuarios = {}
Processador_msg = ProcessadorMensagens()

def detectarIntent(usuario, texto): 
    param = {'v': 20150910}

    dados_json = {
                "lang": "pt-br",
                "query": texto,
                "sessionId": usuario.user_id,
                "resetContexts": "true",
                "contexts": []
              }
    for contexto in usuario.contexto:
        dados_json['contexts'].append({'name': contexto, 'lifespan': 2, 'parameters': {}})

    response = requests.post(
        DIALOGFLOW_CONFIG['URL'],
        params=param,
        json=dados_json,
        headers={'Authorization': 'Bearer '  + DIALOGFLOW_CONFIG['token']}
    )

    print(str( response.json()))
    return response.json()



def enviar_mensagem(payload):

    auth = {'access_token': FACEBOOK_PAGE_ACCESS_TOKEN}
    response = requests.post(
        FB_API_URL,
        params=auth,
        json=payload
    )

    print(str( response.json()))
    return response.json()

#verificação inicial do facebook
@app.route("/", methods=['GET'])
def verificar_bot():

    if request.args.get("hub.verify_token") == FACEBOOK_VERIFY_TOKEN:
        return request.args.get("hub.challenge")
    return 'bot on'

#mensagem recebida do facebook
@app.route("/", methods=['POST'])
def recebe_mensagem():
    payload = request.json

    for evento in payload['entry']:
        print("teste " + str(evento))
        if 'messaging' in evento:
            for mensagem in evento['messaging']:
                sender_id = mensagem['sender']['id']
                
                if sender_id not in Usuarios:
                    Usuarios[sender_id] = Usuario(sender_id)

                if 'message' in mensagem:
                    texto = mensagem['message']['text']
                    print('Mensagem do usuario(' + str(sender_id) + '): ' + texto)
                    DialogflowJSON = detectarIntent( Usuarios[sender_id], texto)
                    Processador_msg.ProcessaMensagem(Usuarios[sender_id], DialogflowJSON)
                    
                elif 'postback' in mensagem:
                    print('postback recebido. payload: ' + mensagem['postback']['payload'])
                    Processador_msg.ProcessaPostback(Usuarios[sender_id], mensagem['postback']['payload'])
                    

                JSONResposta = Usuarios[sender_id].filaMensagens;
                for resposta in JSONResposta:
                    resposta['recipient']['id'] = sender_id
                    enviar_mensagem(resposta)
                Usuarios[sender_id].filaMensagens = []

    return 'ok'

#invoca o scraper para atualizar o banco de dados
@app.route('/AtualizarDados', methods=['GET'])
def AtualizarDadosURL():
    AtualizarBanco()
    return "dados atualizados"

#um teste da pesquisa
@app.route('/PesquisaTeste', methods=['GET'])
def PesquisaTeste():
    respostas = []
    for website in scrapersDisponiveis.keys():
        dados = scrapersDisponiveis[website](request.args.get('Busca'))
        respostas.append(dados)
    return "<html><body><pre>" + json.dumps(respostas, sort_keys=True, indent=4) + "</pre></body></html>"

@app.route('/resumo/<chave>', methods=['GET'])
def getResumo(chave):
    dados = Processador_msg.resumosFinalizados[chave]

    _total = 0.0
    for item in dados:
        _total += item['Preco']
    return render_template('resumo.html', titulo = chave,  itens = dados, total = _total)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 5000)))