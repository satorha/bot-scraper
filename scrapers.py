import urllib.parse
import urllib.request
from bs4 import BeautifulSoup
import re


def GetPrecosKabum(busca):
    url = 'https://www.kabum.com.br/cgi-local/site/listagem/listagem.cgi?'
    params = {
        'string': busca,  
        'limite': 100
    }

    urlBusca = url + urllib.parse.urlencode(params)
    
    print('Executando busca \"' + busca + '\" pela url \"' + url + "\n")

    response = urllib.request.urlopen(urlBusca)
    DOM = BeautifulSoup(response)
    itensDOM = DOM.findAll('div', {'class': 'listagem-box'})

    listaItens = []

    for itemAtual in itensDOM:
        nome        = itemAtual.find('span', {'class':'H-titulo'}).a.string 
        preco = ''
        url = itemAtual.find('span', {'class':'H-titulo'}).a['href']

        link_imagem = itemAtual.find('div', {'class': 'listagem-img'}).a.img['src']

        ElementoAVista = itemAtual.find('div', {'class':'listagem-preco'})
        ElementoAPrazo = itemAtual.find('div', {'class':'listagem-precoavista'})

        if ElementoAVista != None:
            preco = ElementoAVista.string
        elif ElementoAPrazo != None:
            preco = ElementoAPrazo.string
        else:
            print("Não foi possível obter o preço de um item.")
            continue

        string_numero = re.findall('[0-9]+[,.][0-9][0-9]', preco.replace('.', ''))[0].replace(',', '.')
        if len(string_numero) == 0:
            print("não foi possível obter o preço de um item.")
            continue

        listaItens.append({'Nome': nome, 'Preco': float(string_numero), 'URL': url, 'Loja': 'Kabum', 'Imagem': link_imagem})
    print('Encontrado ' + str(len(listaItens)) + 'itens')
    return listaItens

scrapersDisponiveis = {
    'kabum': GetPrecosKabum
}
